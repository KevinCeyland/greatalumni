<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Actualite::class, function (Faker $faker) {
    return [
        'content' => $faker->sentence($nbWords = 6, $variableNbWords = true),
        'media' => $faker->imageUrl($width = 640, $height = 480),
        'typeMedia' => $faker->word,
        'user_id' => 1,
    ];
});
