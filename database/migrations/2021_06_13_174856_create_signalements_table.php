<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignalementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signalements', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id_signaler')->unsigned()->nullable();
            $table->foreign('user_id_signaler')->references('id')->on('users');
            $table->integer('user_id_signaleur');
            $table->string('typeSignalement');
            $table->integer('id_model_signaler')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signalements');
    }
}
