<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table topics
    protected $guarded = [];
    // Relation One to Many avec la table posts (un topic à plusieurs posts)
    public function posts()
    {
        return $this->hasMany("App\Post");
    }
    // Relation Many to One avec la table sujet (plusieurs topics peuvent être dans un sujet)
    public function sujet()
    {
        return $this->belongsTo("App\Sujet");
    }
    // Relation Many to One avec la table users (plusieurs topic peuvent être créer par un utilisateur)
    public function user()
    {
        return $this->belongsTo("App\User");
    }
}
