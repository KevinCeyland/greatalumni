<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParcoursPro extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table parcourspros
    protected $guarded = [];
    // Relation Many to One avec la table users (plusieurs parcours pros appartiennent à un seul utilisateur)
    public function user () {
        return $this->belongsTo("App\User");
    }
}
