<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table messages
    protected $guarded = [];
    // Relation Many to One avec la table conversations (plusieurs message appartiennent à une conversation)
    public function conversation() {
        return $this->belongsTo("App\Conversation");
    }
    // Relation Many to One avec la table users (plusieurs message appartiennent à un utilisateur)
    public function user() {
        return $this->belongsTo('App\User');
    }
}
