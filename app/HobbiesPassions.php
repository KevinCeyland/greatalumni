<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HobbiesPassions extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table hobbiesPassions
    protected $guarded = [];
    // Relation Many to One avec la table users (plusieurs hobbies/passions appartiennent à un seul utilisateur)
    public function user () {
        return $this->belongsTo("App\User");
    }

}
