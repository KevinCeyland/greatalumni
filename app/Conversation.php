<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table conversations
    protected $guarded = [];
    // Relation Many to Many avec la table users (plusieurs utilisateurs peuvent avoir plusieurs conversation et l'inverse également)
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
    // Relation One to Many avec la table messages (une conversation peut avoir plusieurs messages)
    public function messages()
    {
        return $this->hasMany('App\Message')->orderBy('created_at');
    }
}
