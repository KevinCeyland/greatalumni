<?php

namespace App\Http\Controllers;

use App\User;
use App\Actualite;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class ActualiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Fonction permettant de récupérer toutes les actualités trié par id décroissant, avec les commentaires trié par id croissant.
     * Nous avons également une variable $limit qui permet d'avoir un nombre limité d'actualité
     *
     * @param [int] $limit
     * @return array d'actualités
     */
    public function index($limit)
    {
        // Requête permettant de récupérer les actualités et les commentaires triés.
        $actualites = Actualite::orderByDesc('id')->with('comments')->orderBy('id','asc')->limit($limit)->get();
        $array_actualites = json_decode($actualites, true);
        // Nous récupérons tous les utilisateurs de la base
        $users = User::all();

        foreach($actualites as $nb => $val) {
            // Nous comptons les actualités liké, et nous déterminons si l'utilisateur en a liké un, si oui nous afficherons l'icon avec une couleur en front
            $array_actualites[$nb]['like'] = $val->liked->count();
            foreach($val->liked as $like) {
                if($like->pivot->user_id == Auth::user()->id) {
                    $array_actualites[$nb]['like_color'] = true ;
                } else {
                    $array_actualites[$nb]['like_color'] = false ;
                }
            }
            // Nous enverrons l'image de l'actualité grace a une fonction dans notre helper qui converti en base 64 notre image stocker dans Laravel
            $array_actualites[$nb]['media'] = Helper::getImageStringAttribute($val['media'], $val['typeMedia'], "actualites");
            foreach($users as $user) {
                // Pour chaque actualités on enregistre l'auteur du post
                if($user['id'] == $actualites[$nb]['user_id'])  {

                    $array_actualites[$nb]['userAuthor']['nom'] = $user->nom;
                    $array_actualites[$nb]['userAuthor']['prenom'] = $user->prenom;
                    $array_actualites[$nb]['userAuthor']['photo'] = Helper::getImageStringAttribute($user->photo, "image/" . pathinfo($user->photo, PATHINFO_EXTENSION), "photoProfile");
                }
                $comments = json_decode($actualites[$nb]['comments'], true);
                // Pour chaque commentaires on affichera le nom et sa photo
                foreach($comments as $nbb => $comment) {
                    if($user['id'] == $comment['user_id']) {
                        $array_actualites[$nb]['comments'][$nbb]['user']['nomPrenom'] = $user->nom . " " . $user->prenom;
                        $array_actualites[$nb]['comments'][$nbb]['user']['photo'] = Helper::getImageStringAttribute($user->photo, "image/" . pathinfo($user->photo, PATHINFO_EXTENSION), "photoProfile");

                    }
                }

            }

        }
        return response()->json($array_actualites);
    }

    /**
     * Fonction permettant d'enregistrer une nouvelle actualités
     *
     * @param  \Illuminate\Http\Request  $request
     * @return response json avec un message de confirmation
     */
    public function store(Request $request)
    {
        $actualite = new Actualite();
        $req = $request->all();
        $actualite->content = $req['content'];
        // Nous enregistrons le nom du fichier et son extension dans la colonne media de notre table actualités
        // et nous déplaçons l'image physique dans un dossier actualités dans Laravel
        if($request->hasFile("media")) {
            $file = $req['media'];
            $filename = $req['media']->getClientOriginalName();
            $file->move('img/actualites/', $filename);
            $actualite->media = $filename;

        }
        $actualite->typeMedia = $req['typeMedia'];
        $actualite->user_id = Auth::user()->id;
        $actualite->save();
        return response()->json(["message" => "L'actualité à bien été enregistrée !"]);
    }


    /**
     * Fonction permettant de supprimer une actualités
     *
     * @param  type [int] id de l'actualité
     * @return reponse json d'un message  de confirmation
     */
    public function destroy($id)
    {
        $actualite = Actualite::find($id);
        File::delete($actualite->media);
        $actualite->delete();
        return response()->json(["message" => "L'actualite a bien été supprimée !"]);
    }
}
