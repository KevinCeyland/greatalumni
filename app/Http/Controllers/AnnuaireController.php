<?php

namespace App\Http\Controllers;

use App\User;
use App\ParcoursPro;
use App\Helpers\Helper;
use App\HobbiesPassions;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AnnuaireController extends Controller
{
    /**
     * Fonction permettant de récupérer tous les utilisateurs de l'annuaire.
     * Nous avons également une variable $limit qui permet d'avoir un nombre limité d'utilisateurs
     *
     * @param [int] $limit
     * @return array d'utilisateurs
     */
    public function index($limit)
    {
        // Requête permettant de récupérer un certains nombre d'utilisateurs limité
        $users = User::orderByDesc('id')->limit($limit)->get();
        // On crée un tableau qui nous permettra d'envoyer les bonnes données pour la compréhension de VueJS
        $arrayUsers = array();
        foreach ($users as $key => $value) {
            $hobbiesPassions = HobbiesPassions::where('user_id', '=', $value['id'])->get();
            $parcoursPro = ParcoursPro::where("user_id", "=", $value['id'])->get();
            $arrayUsers[$key]['id'] = $value['id'];
            $arrayUsers[$key]['nom'] = $value['nom'];
            $arrayUsers[$key]['prenom'] = $value['prenom'];
            $arrayUsers[$key]['anneePromotion'] = $value['anneePromotion'];
            $arrayUsers[$key]['textLibre'] = $value["textLibre"];
            $arrayUsers[$key]['hobbiesPassions'] = json_decode($hobbiesPassions, true);
            $arrayUsers[$key]['parcoursPro'] = json_decode($parcoursPro, true);
            $arrayUsers[$key]['photo'] = Helper::getImageStringAttribute($value['photo'], "image/" . pathinfo($value['photo'], PATHINFO_EXTENSION), "photoProfile");
        }
        return $arrayUsers;
    }
    /**
     * Fonction permettant de rechercher un utilisateur sur la base de son nom ou son prénom ou son année de promotion
     *
     * @param [any] $value
     * @return array d'utilisateur
     */
    public function search($value)
    {
        // Requête permettant de récupérer les utilisateurs en filtrant avec la variable $value
        $users = User::where('nom', 'like', '%' . $value . '%')->orWhere('prenom', 'like', '%' . $value . '%')->orWhere('anneePromotion', 'like', '%' . $value . '%')->get();
  // On crée un tableau qui nous permettra d'envoyer les bonnes données pour la compréhension de VueJS
        $arrayUsers = array();
        foreach ($users as $key => $value) {
            // Cette condition nous permet de ne pas afficher l'utilisateur connecté dans la recherche
            if ($value['id'] == Auth::user()->id) {
            } else {
                $hobbiesPassions = HobbiesPassions::where('user_id', '=', $value['id'])->get();
                $parcoursPro = ParcoursPro::where("user_id", "=", $value['id'])->get();
                $arrayUsers[$key]['id'] = $value['id'];
                $arrayUsers[$key]['nom'] = $value['nom'];
                $arrayUsers[$key]['prenom'] = $value['prenom'];
                $arrayUsers[$key]['photo'] = Helper::getImageStringAttribute($value['photo'], "image/" . pathinfo($value['photo'], PATHINFO_EXTENSION), "photoProfile");
                $arrayUsers[$key]['anneePromotion'] = $value['anneePromotion'];
                $arrayUsers[$key]['textLibre'] = $value["textLibre"];
                $arrayUsers[$key]['hobbiesPassions'] = json_decode($hobbiesPassions, true);
                $arrayUsers[$key]['parcoursPro'] = json_decode($parcoursPro, true);
            }
        }
        return $arrayUsers;
    }
}
