<?php

namespace App\Http\Controllers;

use App\User;
use App\Actualite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReactionController extends Controller
{
    /**
     * Fonction permettant de liker une actualité
     *
     * @param [int] $actualite
     * @return int le nombre de like sur l'actualité
     */
    public function likeActuPartage($actualite){
        $user = User::find(Auth::user()->id);
        $user->reactionUser()->toggle($actualite);
        $actualiteNewNBLikes = Actualite::find($actualite);
        return $actualiteNewNBLikes->liked()->count();
    }
}
