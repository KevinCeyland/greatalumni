<?php

namespace App\Http\Controllers;

use App\User;
use App\Actualite;
use App\Commentaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    // Pour accéder aux fonctions du controller il faut être authentifier
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }
    /**
     * Fonction permettant d'enregistrer un commentaire sur une actualité
     *
     * @param [int] $id de l'actualité
     * @param Request $request
     * @return response json d'un message de confirmation et le commentaire créer
     */
    public function storeCommentActu($id, Request $request)
    {
        // On transforme l'objet $request en array des données envoyer par l'utilisateur
        $req = $request->all();
        $actualite = Actualite::find($id);
        $comment = new Commentaire();
        $comment->message = $req['comment'];
        $comment->user_id = Auth::user()->id;
        // On sauvegarde le commentaire et on le lie directement à l'actualité avec la relation one to many de Laravel
        $actualite->comments()->save($comment);
        return response()->json(["message" => "Commentaires bien posté", "comment" => $comment]);
    }
    /**
     * Fonction permettant de supprimer un commentaire
     *
     * @param [int] $id du commentaire
     * @return response json d'un message de confirmation
     */
    public function deleteCommentActu($id) {
        $comment = Commentaire::find($id);
        $comment->delete();
        return response()->json(['message'=>'Votre commentaires à été supprimé !']);
    }
}
