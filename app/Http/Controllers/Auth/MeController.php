<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MeController extends Controller
{
    // Pour accéder à la fonction il faut être authentifier
    public function __construct()
    {
        $this->middleware(['auth:api']);
    }
    /**
     * Fonction permettant de vérifier si l'utilisateur est connecté grâce au token
     *
     * @param Request $request
     * @return response json des infos de l'utilisateur connecté
     */
    public function __invoke(Request $request)
    {
        $user = $request->user();
        $photo = Helper::getImageStringAttribute($user->photo, "image/" . pathinfo($user->photo, PATHINFO_EXTENSION), "photoProfile");
        return response()->json([
            'email' => $user->email,
            'nom' => $user->nom,
            'prenom' => $user->prenom,
            'id' => $user->id,
            'photo' => $photo,
            'departement' => $user->departement,
            'anneePromotion' => $user->anneePromotion,
            'textLibre' => $user->textLibre,
            'access_messagerie' => $user->access_messagerie,
            'roles' => $user->roles,
        ]);
    }
}
