<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SignInController extends Controller
{
    /**
     * Fonction permettant de se connecter à l'API
     *
     * @param Request $request
     * @return response json du token
     */
    public function __invoke(Request $request)
    {
       // Si la demande d'authentification est bonne, cela renvoie un token que l'on stocke dans la variable token sinon la variable sera false
       $token = auth()->attempt($request->only('email', 'password'));
       // Si le token est false nous retournons une réponse null est une erreur 498
       if(!$token) {
           return response()->json(["message"=>"Vos identifiants sont incorrect", "code"=>498]);
       }

       // Nous retournons en format JSON le token.
       return response()->json([compact('token')]);
    }
}
