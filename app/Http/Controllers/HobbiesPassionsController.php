<?php

namespace App\Http\Controllers;

use App\HobbiesPassions;
use Illuminate\Http\Request;

class HobbiesPassionsController extends Controller
{
    /**
     * Fonction permettant de lister les hobbies et passions d'un utilisateur grâce à son id
     *
     * @param [int] $id
     * @return array des hobbies et passions
     */
    public function index ($id) {
        $hobbbiesPassions = HobbiesPassions::where("user_id", "=", $id)->get();
        $arrayHobbbiesPassions = array();
        foreach ($hobbbiesPassions as $key => $value) {
            $arrayHobbbiesPassions[$key]['id'] = $value['id'];
            $arrayHobbbiesPassions[$key]['intitule'] = $value['intitule'];
            $arrayHobbbiesPassions[$key]['user_id'] = $value['user_id'];
        }
        return $arrayHobbbiesPassions;

    }
    /**
     * Fonction permettant de créer un nouvelle hobbie/passion
     *
     * @param Request $request
     * @return array de l'hobbie/passion
     */
    public function store (Request $request) {
        $hobbbiesPassions = new HobbiesPassions();
        $hobbbiesPassions->intitule = $request->intitule;
        $hobbbiesPassions->user_id = $request->user_id;
        $hobbbiesPassions->save();
        $arrayHobbbiesPassions = array();
        foreach ($hobbbiesPassions as $key => $value) {
            $arrayHobbbiesPassions[$key]['id'] = $value['id'];
            $arrayHobbbiesPassions[$key]['intitule'] = $value['intitule'];
        }
        return $arrayHobbbiesPassions;
    }
    /**
     * Fonction permettant de mettre à jour un hobbie/passion
     *
     * @param Request $request
     * @param [int] $id du hobbie/passion
     * @return array de l'hobbie/passion mis à jour
     */
    public function update (Request $request, $id) {
        $hobbbiesPassions = HobbiesPassions::find($id);
        $hobbbiesPassions->intitule = $request->intitule;
        $hobbbiesPassions->user_id = $request->user_id;
        $hobbbiesPassions->save();
        $arrayHobbbiesPassions = array();
        foreach ($hobbbiesPassions as $key => $value) {
            $arrayHobbbiesPassions[$key]['id'] = $value['id'];
            $arrayHobbbiesPassions[$key]['intitule'] = $value['intitule'];
        }
        return $arrayHobbbiesPassions;
    }
    /**
     * Fonction permettant de supprimer un hobbie/passion
     *
     * @param [int] $id
     * @return void
     */
    public function delete ($id) {
        $hobbbiesPassions = HobbiesPassions::find($id);
        $hobbbiesPassions->delete();
        return response()->json(['message'=>"Suppréssion réussi !"]);
    }
}
