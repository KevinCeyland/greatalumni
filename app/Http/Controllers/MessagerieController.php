<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use App\Conversation;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class MessagerieController extends Controller
{
    /**
     * Fonction permettant de crée une nouvelle conversation
     * Nous récupérons l'id de l'utilisateur avec qui l'utilisateur connecté veut discuté
     *
     * @param [int] $idSecondUser
     * @return int l'id de la conversation
     */
    public function createConversations($idSecondUser)
    {
        // On récupère l'utilisateur tier (avec ses conversations) avec qui l'utilisateur connecté veut discuté
        $secondUser = User::with("conversations")->where("id", "=", $idSecondUser)->get();
        // A cause de la condition where nous récupérons un id, donc on s'en débarrasse en pointant sur l'index 0
        $secondUser = $secondUser[0];
        // On créer une nouvelle conversation
        $conversationEnCommun = new Conversation();
        foreach ($secondUser->conversations as $key => $value) {
            // Dans cette requête nous cherchons toutes les conversations dans lequel l'utilisateur tier à participé
            $conversations = Conversation::select('id')->whereHas("users", function ($q) use ($value) {
                $q->where('users.id', "=", $value->pivot->user_id);
            })->get();
            // Dans ses conversations nous allons cherché si l'utilisateur connecter à une conversation en commun avec lui
            foreach ($conversations as $key => $value) {
                $conversationEnCommun = Conversation::whereHas("users", function ($q) use ($value) {
                    $q->where('users.id', "=", Auth::user()->id);
                })->where('id', "=", $value['id'])->get();
                foreach ($conversationEnCommun as $key => $val) {
                    if ($value['id'] == $val['id']) {
                        // Si ils ont une conversations en commun nous récupérons l'id de la conversation et nous la renvoyons
                        $idConversation = $conversationEnCommun[0]->id;
                        return $idConversation;
                    }
                }
            }
        }
        // Si il nous aucune conversation en commun nous en créeons une et nous renvoyons l'id de cette nouvelle conversation
        if ($conversationEnCommun->count() == 0) {
            $conversation = new Conversation();
            $conversation->save();
            $conversation->users()->attach(Auth::user()->id);
            $conversation->users()->attach($idSecondUser);
            return $conversation->id;
        }
    }
    /**
     * Fonction permettant d'enregistrer un message dans la conversation actuel
     *
     * @param Request $request
     * @param [int] $idConversation
     * @return array du message
     */
    public function storeMessage(Request $request, $idConversation)
    {
        // Nous cherchons la conversation dans laquel le message va être enregistré
        $conversation = Conversation::find($idConversation);
        $message = new Message();
        $message->contenu = $request->content;
        $message->conversation_id = $conversation->id;
        $message->user_id = $request->participantId;
        $message->save();

        $arrayMessage = array();
        $arrayMessage['id'] = $message->id;
        $arrayMessage['content'] = $message->contenu;
        $arrayMessage['timestamp'] = $message->created_at;
        if (Auth::user()->id == $message->user_id) {
            $arrayMessage['myself'] = true;
            $arrayMessage['participantId'] = $message->user_id;
        } else {
            $arrayMessage['participantId'] = $message->user_id;
        }
        $arrayMessage['id'] = $message->id;
        $arrayMessage["contenu"] = $message->contenu;
        $arrayMessage["conversation_id"] = $message->conversation_id;

        return $arrayMessage;
    }
    /**
     * Fonction permettant de listé tous les messages d'une conversation
     *
     * @param [int] $idConversation
     * @return array de tous les messages de la conversation
     */
    public function listeMessage($idConversation)
    {
        // Nous cherchons la conversation
        $conversation = Conversation::find($idConversation);
        // Requête permettant de récupérer tous les messages qui ont l'id de la conversation trouvé
        $messages = Message::where('conversation_id', "=", $conversation->id)->get();
        // Nous cherchons ensuite le participant de la conversation grâce a une requête, il ne peut avoir que deux personnes dans une conversation
        // Donc le dernier where nous sert à ne pas nous selectionner.
        $participant = DB::table('conversation_user')->where('conversation_id', "=", $conversation->id)->where('user_id', "!=", Auth::user()->id)->get();

        // Nous enregistrons dans des variables l'id du participant et de l'utilisateur connecté
        $myself = Auth::user()->id;
        $participantId = $participant[0]->user_id;

        $arrayMessages = array();
        foreach ($messages as $key => $value) {
            $arrayMessages['messages'][$key]['id'] = $value['id'];
            $arrayMessages['messages'][$key]['content'] = $value['contenu'];
            $arrayMessages['messages'][$key]['timestamp'] = $value['created_at'];

            if (Auth::user()->id == $value['user_id']) {
                // Si le message est de nous, la clef myself du tableau sera a true sinon elle n'existera pas
                $arrayMessages['messages'][$key]['myself'] = true;
                $arrayMessages['messages'][$key]['participantId'] = $value['user_id'];
                $myself = $value['user_id'];
            } else {
                $arrayMessages['messages'][$key]['participantId'] = $value['user_id'];
                $participant = $value['user_id'];
            }
        }
        // Nous cherchons ensuite l'utilisateur dans la base de données grâce à son id
        $userParticipant = User::find($participantId);
        // Pareil pour l'utilisateur connecté
        $authUser = User::find($myself);
        // Nous créons un array parfaitement interprétable par le package "Vue Quick Chat" dans notre front
        $arrayMessages['myself'] = array("id" => $authUser->id, "name" => $authUser->nom . ' ' . $authUser->prenom, "profilePicture" => Helper::getImageStringAttribute($authUser->photo, "image/" . pathinfo($authUser->photo, PATHINFO_EXTENSION), "photoProfile"));
        $arrayMessages['participants'] = array("id" => $userParticipant->id, "name" => $userParticipant->nom . ' ' . $userParticipant->prenom,  "profilePicture" => Helper::getImageStringAttribute($userParticipant->photo, "image/" . pathinfo($userParticipant->photo, PATHINFO_EXTENSION), "photoProfile"));


        return $arrayMessages;
    }
    /**
     * Fonction permettant d'avoir une liste de tous les utilisteurs
     *
     * @return array d'utilisateurs
     */
    public function listeUsersOrderByLastMessage()
    {
        $users = User::where("id", "!=", Auth::user()->id)->with("messages")->get();
        foreach ($users as $key => $value) {

            $arrayUsers[$key]['id'] = $value['id'];
            $arrayUsers[$key]['nom'] = $value['nom'];
            $arrayUsers[$key]['prenom'] = $value['prenom'];
            $arrayUsers[$key]['photo'] = Helper::getImageStringAttribute($value['photo'], "image/" . pathinfo($value['photo'], PATHINFO_EXTENSION), "photoProfile");
        }
        return response()->json($arrayUsers);
    }
}
