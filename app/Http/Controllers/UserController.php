<?php

namespace App\Http\Controllers;

use App\User;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Fonction permettant de récupérer le profil de l'utilisateur connecté
     *
     * @return array de l'utilisateur connecté
     */
    public function profil() {
        $user = Auth::user();
        $arrayUser = array();
        $arrayUser['id'] = $user->id;
        $arrayUser['nom'] = $user->nom;
        $arrayUser['prenom'] =$user->prenom;
        $arrayUser['email'] = $user->email;
        $arrayUser['departement'] = $user->departement;
        $arrayUser['anneePromotion'] = $user->anneePromotion;
        $arrayUser['textLibre'] = $user->textLibre;
        $photo = Helper::getImageStringAttribute($user->photo, "image/" . pathinfo($user->photo, PATHINFO_EXTENSION), "photoProfile");
        $arrayUser['photo'] = $photo;
        return $arrayUser;
    }
    /**
     * Fonction permettant de mettre à jour son profil
     *
     * @param Request $request
     * @return response json d'un message de confirmation et l'utilisateur modifié
     */
    public function updateProfil(Request $request) {
        $req = $request->all();
        $user = User::find(Auth::user()->id);
        $user->id = $req['id'];
        $user->nom = $req['nom'];
        $user->prenom = $req['prenom'];
        $user->departement =$req['departement'];
        $user->anneePromotion = $req['anneePromotion'];
        // Si l'utilisateur à modifier son mot de passe on le hash et l'enregistre dans la colonne password
        if(array_key_exists("password", $req)) {
            $user->password = Hash::make($req['password']);
        }
        $user->textLibre =$req['textLibre'];
        // Si l'utilisateur à modifier sa photo de profil on enregistre dans la colonne photo le nom de l'image et mettons son image dans le dossier
        // imageProfile de Laravel
        if($request->hasFile("photo")) {
            $file = $req['photo'];
            $filename = $req['photo']->getClientOriginalName();
            $file->move('img/photoProfile/', $filename);
            $user->photo = $filename;
        }
        $user->save();
        return response()->json(["message"=>"Votre profile à bien été mis à jour", "user" => $user]);
    }
    /**
     * Fonction permettant d'afficher tous les utilisateurs par ordre décroissant
     *
     * @return response json d'un array d'utilisateurs
     */
    public function index() {
        $users = User::orderByDesc('id')->get();
        return response()->json($users);
    }
     /**
     * Fonction permettant de récupérer tous les utilisateurs.
     * Nous avons également une variable $limit qui permet d'avoir un nombre limité d'utilisateurs
     *
     * @param [int] $limit
     * @return array d'utilisateurs
     */
    public function listeUsers($limit) {
        $users = User::orderByDesc('id')->limit($limit)->get();
        $arrayUsers = array();
        foreach ($users as $key => $value) {
            $arrayUsers[$key]["id"] = $value['id'];
            $arrayUsers[$key]["nom"] = $value['nom'];
            $arrayUsers[$key]['prenom'] = $value['prenom'];
            $arrayUsers[$key]['email'] = $value['email'];
            $arrayUsers[$key]['departement'] = $value['departement'];
            $arrayUsers[$key]['anneePromotion'] = $value['anneePromotion'];
            $arrayUsers[$key]['access_messagerie'] = $value['access_messagerie'];
            $photo = Helper::getImageStringAttribute($value['photo'], "image/" . pathinfo($value['photo'], PATHINFO_EXTENSION), "photoProfile");
            $arrayUsers[$key]['photo'] = $photo;
            $arrayUsers[$key]['roles'] = $value->roles;
            $arrayUsers[$key]['role_id'] = $value->roles[0]->id;
        }
        return response()->json($arrayUsers);
    }
    /**
     * Fonction permettant d'enregistrer un utilisateur
     *
     * @param Request $request
     * @return response json d'un message de confirmation et l'utilisateur créer
     */
    public function store(Request $request) {
        $req = $request->all();
        $user = new User();
        $user->nom = $req['nom'];
        $user->prenom = $req['prenom'];
        $user->departement = $req['departement'];
        $user->anneePromotion = $req['anneePromotion'];
        $user->access_messagerie = $req['access_messagerie'];
        $user->email = $req['email'];
        $user->password = Hash::make($req['password']);
        $user->photo= $req['photo'];
        $user->save();
        $user->roles()->attach($req['role_id']);
        return response()->json(["message" => "L'utilisateur a bien été enregistré", "user" => $user]);
    }
    /**
     * Fonction permettant de modifier un utilisateur
     * @param int $id
     * @param Request $request
     * @return response json d'un message de confirmation et l'utilisateur modifié
     */
    public function update(Request $request, $id) {
        $user = User::find($id);
        $req = $request->all();
        $user->nom=$req['nom'];
        $user->prenom=$req['prenom'];
        $user->departement = $req['departement'];
        $user->access_messagerie = $req['access_messagerie'];
        $user->anneePromotion = $req['anneePromotion'];
        $user->email = $req['email'];
         // Si l'administrateur souhaite modifier son mot de passe on le hash et l'enregistre dans la colonne password
        if(array_key_exists("password", $req)) {
            $user->password = Hash::make($req['password']);
        }
        $user->photo=$req['photo'];
        $user->save();
        $user->roles()->detach();
        $user->roles()->attach($req['role_id']);

        return response()->json(["message" => "L'utilisateur a bien été modifié", "user" => $user]);
    }
    /**
     * Fonction permettant de supprimé un utilisateur
     *
     * @param [type] $id
     * @return response json d'un message de confirmation
     */
    public function delete($id) {
        $user = User::find($id);
        // Fonction permettant de délié un rôle d'un utilisateur
        $user->roles()->detach();
        $user->delete();
        return response()->json(["message" => "L'utilisateur a bien été supprimé"]);
    }
}
