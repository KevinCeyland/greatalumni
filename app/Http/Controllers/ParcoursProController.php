<?php

namespace App\Http\Controllers;

use App\ParcoursPro;
use Illuminate\Http\Request;

class ParcoursProController extends Controller
{
     /**
     * Fonction permettant de lister les parcours pro d'un utilisateur grâce à son id
     *
     * @param [int] $id
     * @return array des parcours pro
     */
    public function index ($id) {
        $parcoursPros = ParcoursPro::where("user_id", "=", $id)->get();
        $arrayParcoursPro = array();
        foreach ($parcoursPros as $key => $value) {
            $arrayParcoursPro[$key]['id'] = $value['id'];
            $arrayParcoursPro[$key]['intitule'] = $value['intitule'];
            $arrayParcoursPro[$key]['user_id'] = $value['user_id'];
        }
        return $arrayParcoursPro;

    }
     /**
     * Fonction permettant de créer un nouveau parcours pro
     *
     * @param Request $request
     * @return array du parcours pro
     */
    public function store (Request $request) {
        $parcoursPros = new ParcoursPro();
        $parcoursPros->intitule = $request->intitule;
        $parcoursPros->user_id = $request->user_id;
        $parcoursPros->save();
        $arrayParcoursPro = array();
        foreach ($parcoursPros as $key => $value) {
            $arrayParcoursPro[$key]['id'] = $value['id'];
            $arrayParcoursPro[$key]['intitule'] = $value['intitule'];
        }
        return $arrayParcoursPro;
    }
    /**
     * Fonction permettant de mettre à jour un parcours pro
     *
     * @param Request $request
     * @param [int] $id du hparcours pro
     * @return array du parcours pro
     */
    public function update (Request $request, $id) {
        $parcoursPros = ParcoursPro::find($id);
        $parcoursPros->intitule = $request->intitule;
        $parcoursPros->user_id = $request->user_id;
        $parcoursPros->save();
        $arrayParcoursPro = array();
        foreach ($parcoursPros as $key => $value) {
            $arrayParcoursPro[$key]['id'] = $value['id'];
            $arrayParcoursPro[$key]['intitule'] = $value['intitule'];
        }
        response()->json(['message'=>"Modification réussi !"]);
    }
     /**
     * Fonction permettant de supprimer un parcours pro
     *
     * @param [int] $id
     * @return void
     */
    public function delete ($id) {
        $parcoursPros = ParcoursPro::find($id);
        $parcoursPros->delete();
        return response()->json(['message'=>"Suppréssion réussi !"]);
    }
}
