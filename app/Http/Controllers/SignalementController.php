<?php

namespace App\Http\Controllers;

use App\User;
use App\Topic;
use App\Signalement;
use App\Conversation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SignalementController extends Controller
{
    /**
     * Fonction permettant de signaler un utilisateur
     *
     * @param Request $request
     * @param [int] $id
     * @return response json d'un message de confirmation
     */
    public function report(Request $request, $id) {
        $signalements = Signalement::all();
        foreach ($signalements as $key => $value) {
            // Nous verifions que l'utilisateur n'a pas signalé deux fois le même utilisateur dans la même situation (même conversation ou même topic de forum)
            if($value->user_id_signaleur == Auth::user()->id && $value->user_id_signaler == $id && $value->typeSignalement == $request->typeSignalement) {
                return response()->json(["message"=>"Vous ne pouvez pas signaler deux fois un utilisateur", "type"=>"error"]);
            }
        }
        // Nous cherchons l'utilisateur qui à été signaler et créons un nouveau signalement
        $user = User::find($id);
        $signalement = new Signalement();
        $signalement->user_id_signaler = $user->id;
        $signalement->user_id_signaleur = Auth::user()->id;
        $signalement->typeSignalement = $request->typeSignalement;
        $signalement->id_model_signaler = $request->idModelSignaler;
        $signalement->save();
        return response()->json(["message"=>"L'utilisateur à bien été signaler, merci pour votre contribution !", "type"=>"success"]);
    }
    /**
     * Liste de tous les signalements de l'application
     * Nous avons également une variable $limit qui permet d'avoir un nombre limité de signalements
     *
     * @param [int] $limit
     * @return array de signalements
     */
    public function listReportUser($limit) {
        $signalements = Signalement::orderByDesc('id')->limit($limit)->get();
        $arraySignalements = array();
        foreach ($signalements as $key => $value) {
            $arraySignalements[$key]['id'] = $value['id'];
            // On affiche les noms des utilisateurs signalé et signaleur dans leurs tableaux respectif
            $userSignaler = User::find($value['user_id_signaler']);
            $arraySignalements[$key]['nomPrenomSignaler'] = $userSignaler->nom . " " . $userSignaler->prenom;
            $userSignaleur = User::find($value['user_id_signaleur']);
            $arraySignalements[$key]['nomPrenomSignaleur'] = $userSignaleur->nom . " " . $userSignaleur->prenom;
            // Si le type de signalement porte sur un forum ou sur la messagerie on cherche le topic signalé
            if($value['typeSignalement'] == "Forum") {
                $model = Topic::find($value['id_model_signaler']);
            }
            else {
                $model = Conversation::find($value['id_model_signaler']);
            }
            // On enregistre l'id du model signalé dans le tableau ainsi que le type de signalement
            $arraySignalements[$key]['elementSignalerId'] = $model->id;
            $arraySignalements[$key]['typeSignalement'] = $value['typeSignalement'];

        }
        return $arraySignalements;
    }
    /**
     * Fonction permettant de supprimé un model signalé
     *
     * @param [int] $id
     * @param [string] $typeModel
     * @return response json d'un message de confirmation
     */
    public function deleteModel($id, $typeModel) {
        // On cherche en fonction du type du model le bon élément
        if($typeModel == "Forum") {
            $model = Topic::find($id);
        } else {
            $model = Conversation::find($id);
        }
        // Nous récupérons tous les signalements
        $signalements = Signalement::all();
        // Pour chaque signalements si nous trouvons l'id du model signalé nous le supprimons de la table signalements
        foreach ($signalements as $key => $value) {
            if($value['id_model_signaler'] == $id) {
                $value->delete();
            }
        }
        // Nous supprimons ensuite le model qui à été signalé
        $model->delete();
        return response()->json(["message"=>"L'élément à bien été supprimé !"]);
    }
}
