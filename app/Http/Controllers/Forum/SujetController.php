<?php

namespace App\Http\Controllers\Forum;

use App\Sujet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SujetController extends Controller
{
    /**
     * Fonction permettant de récupérer les sujets du forums
     *
     * @return array des sujets
     *
     */
    public function index () {
        // On récupère tous les sujets et on les envois à VueJS avec un tableau
        $sujets = Sujet::all();
        $arraySujets = array();
        foreach ($sujets as $key => $value) {
            $arraySujets[$key]['id'] = $value['id'];
            $arraySujets[$key]['titre'] = $value['libelle'];
            $arraySujets[$key]['icon'] = $value['icon'];
        }
        return $arraySujets;

    }

}
