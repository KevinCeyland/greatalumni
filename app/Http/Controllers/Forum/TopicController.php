<?php

namespace App\Http\Controllers\Forum;

use App\Sujet;
use App\Topic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{
    /**
     * Fonction permettant d'avoir la liste des topics du forum grâce à l'id du sujet.
     *
     * @param [int] $id
     * @return array des topics
     */
    public function index($id)
    {
        // On récupère tous les topics qui ont l'id du sujet passé en paramètre
        $topics = Topic::where("sujet_id", "=", $id)->get();
        $sujet = Sujet::find($id);
        // On crée un tableau qui nous permettra d'envoyer les bonnes données pour la compréhension de VueJS
        $arrayTopics = array();
        $arrayTopics['nomSujet'] = $sujet->libelle;
        $arrayTopic['topics'] = array();
        foreach ($topics as $key => $value) {
            $arrayTopics['topics'][$key]['id'] = $value['id'];
            $arrayTopics['topics'][$key]['titre'] = $value['titre'];
            $arrayTopics['topics'][$key]['contenu'] = $value['contenu'];
            $arrayTopics['topics'][$key]['user'] = array("nomPrenom" => $value->user->nom . " " . $value->user->prenom, "id" => $value->user->id);
            $arrayTopics['topics'][$key]['date'] = $value['created_at'];
            // En fonction du nom du sujet on affichera les cards des topics du certaines couleur
            switch ($sujet->libelle) {
                case 'Juridique':
                    $arrayTopics['topics'][$key]['color'] = "#782253";
                    break;
                case 'Emploi':
                    $arrayTopics['topics'][$key]['color'] = "#1A878E";
                    break;
                case 'Stage':
                    $arrayTopics['topics'][$key]['color'] = "#662382";
                    break;
                case 'Divers':
                    $arrayTopics['topics'][$key]['color'] = "#9DA38B";
                    break;

                default:
                    # code...
                    break;
            }
        }
        return $arrayTopics;
    }
    /**
     * Fonction permettant d'enregistrer un nouveau topic
     *
     * @param Request $request
     * @return response json avec un message de confirmation et le topic créer.
     */
    public function store(Request $request)
    {
        // Création d'un nouveau topic, enregistrement des valeurs saisie dans la requête de l'utilisateur
        // et enregistrement
        $topic = new Topic();
        $topic->titre = $request->titre;
        $topic->contenu = $request->contenu;
        $topic->user_id = Auth::user()->id;
        $topic->sujet_id = $request->sujet_id;
        $topic->save();
        // On crée un tableau qui nous permettra d'envoyer les bonnes données pour la compréhension de VueJS
        $arrayTopic = array();
        $arrayTopic['id'] = $topic->id;
        $arrayTopic['user'] = $topic->user->nom . " " . $topic->user->prenom;
        $arrayTopic['titre'] = $topic->titre;
        $arrayTopic['contenu'] = $topic->contenu;
        return response()->json(['message' => "Votre topic à bien été créer !", "topic" => $arrayTopic]);
    }
}
