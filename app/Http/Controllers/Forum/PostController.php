<?php

namespace App\Http\Controllers\Forum;

use App\Post;
use App\Topic;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Fonction permettant d'afficher la liste des posts d'un topic grâce à l'id de celui-c
     *
     * @param [int] $id
     * @return array
     */
    public function index($id)
    {
        // Requête qui nous permet de récupérer tous les posts avec l'id du topic et de les trié par id croissant
        $posts = Post::where('topic_id', "=", $id)->orderBy('id', 'asc')->get();
        $nomTopic = Topic::find($id);
        // On crée un tableau qui nous permettra d'envoyer les bonnes données pour la compréhension de VueJS
        $arrayPosts = array();
        $arrayPosts['nomTopic'] = $nomTopic->titre;
        $arrayPosts['posts'] = array();
        foreach ($posts as $key => $value) {
            $arrayPosts['posts'][$key]['id'] = $value['id'];
            $arrayPosts["posts"][$key]['user'] = $value->user->nom . " " . $value->user->prenom;
            $arrayPosts['posts'][$key]['photo'] = Helper::getImageStringAttribute($value->user->photo, "image/" . pathinfo($value->user->photo, PATHINFO_EXTENSION), "photoProfile");
            $arrayPosts['posts'][$key]['contenu'] = $value['contenu'];
            $arrayPosts['posts'][$key]['compteurSignalement'] = $value['compteurSignalement'];
            $arrayPosts['posts'][$key]['dateCreation'] = $value['created_at'];
        }
        return $arrayPosts;
    }
    /**
     * Fonction permettant d'enregistrer un post
     *
     * @param Request $request
     * @return response json avec un message de confirmation et le post crée
     */
    public function store(Request $request)
    {

        // Création d'un nouveau post, enregistrement des valeurs saisie dans la requête de l'utilisateur
        // et enregistrement
        $post = new Post();
        $post->contenu = $request->contenu;
        $post->user_id = Auth::user()->id;
        $post->topic_id = $request->topic_id;
        $post->save();
        // On créer un tableau qui nous permettra d'envoyer les bonnes données pour la compréhension de VueJS
        $arrayPosts = array();
        $arrayPosts['id'] = $post->id;
        $arrayPosts['user'] = $post->user->nom . " " . $post->user->prenom;
        $arrayPosts['avatar'] = $post->user->photo;
        $arrayPosts['contenu'] = $post->contenu;
        $arrayPosts['compteurSignalement'] = $post->compteurSignalement;
        $arrayPosts['dateCreation'] = $post->created_at;
        return response()->json(['message' => "Votre post à bien été envoyé !", "post" => $arrayPosts]);
    }
}
