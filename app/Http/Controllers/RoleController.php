<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Fonction permettant de récupérer l'ensemble des rôles
     *
     * @return Objet de roles
     */
    public function index()
    {
        $roles = Role::all();
        return response()->json($roles);

    }
    /**
     * Fonction permettant d'enregistrer un rôle
     *
     * @param Request $request
     * @return response json d'un message de confirmation
     */
    public function store(Request $request)
    {
        Role::create($request->all());
        return response()->json(['message'=>"Rôle bien enregistrée !"]);
    }
    /**
     * Fonction permettant de mettre à jour un rôle
     *
     * @param Request $request
     * @param [int] $id
     * @return response json d'un message de confirmation
     */
    public function update(Request $request, $id)
    {
        $role = Role::find($id);
        $role->libelle = $request->libelle;
        $role->save();
        return response()->json(['message'=>"Rôle bien modifiée !"]);

    }
    /**
     * Fonction permettant de supprimer un rôle
     *
     * @param [int] $id
     * @return response json d'un message de confirmation
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        return response()->json(['message'=>" Rôle bien supprimé !"]);
    }
}
