<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actualite extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table actualités
    protected $guarded = [];
    // Relation One to Many avec la table users (plusieurs actualités appartiennent à un utilisateur)
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    // Relation Many to Many avec la table users permettant d'enregistré les likes (table de jointure réactions)
    public function liked()
    {
        return $this->belongsToMany('App\User','reactions');
    }
    // Relation One to Many avec la table comments (plusieurs commentaires appartiennent à une actualité)
    public function comments()
    {
        return $this->morphMany('App\Commentaire', 'commentable')->latest();
    }
}
