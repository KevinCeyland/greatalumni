<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table parcourspros
    protected $guarded = [];
    // Relation Many to One avec la table topics (plusieurs posts appartiennent à un topic)
    public function topic () {
        return $this->belongsTo("App\Topic");
    }
    // Relation Many to One avec la table users (plusieurs posts appartiennent à un utilisateur)
    public function user() {
        return $this->belongsTo("App\User");
    }
}
