<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Signalement extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table roles
    protected $guarded = [];
    // Relation Many to One avec la table users (plusieurs signalements peuvent être fait par un utilisateur)
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
