<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sujet extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table sujets
    protected $guarded = [];
    // Relation One to Many avec la table topics (un sujet peut avoir plusieurs topics)
    public function topics () {
        return $this->hasMany("App\Topic");
    }
}
