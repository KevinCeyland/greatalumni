<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table commentaires
    protected $guarded = [];

    // Relation Many to One avec la table users (plusieurs commentaires peuvent appartennir à un utilisateur)
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
