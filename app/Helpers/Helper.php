<?php

namespace App\Helpers;

use App\Page;
use App\Module;
use App\PropField;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Spatie\Permission\Models\Permission;


class Helper
{
    /**
     * Fonction permettant d'encodé en base 64 une image
     *
     * @param [string] $img
     * @param [string] $type
     * @param [string] $folder
     * @return l'image en base64
     */
    public static function getImageStringAttribute($img, $type, $folder) {
        // Nous cherchons dans le dossier public de Laravel l'image demandé grâce au paramètre $folder qui est le dossier de conservation de l'image
        // Et le nom de l'image
        $data = File::get(public_path("img\\". $folder ."\\" . $img));
        $base64 = 'data:' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }
}
