<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table topics
    protected $guarded = [];

    /**
     * C'est attributs doivent être masqués pour les tableaux.
     * Lors des requêtes certains champs sont caché
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Les attributs qui doivent être convertis en types natifs.
     * le champ email_verified_at doit être forcément du type datetime
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
    // Relation Many to Many avec la table users permettant d'enregistré les likes (table de jointure réactions)
    public function reactionUser()
    {
        return $this->belongsToMany('App\Actualite', 'reactions');
    }
    // Relation Many to Many avec la table users (plusieurs utilisateurs peuvent avoir plusieurs rôles et l'inverse également)
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
    // Fonction permettant de vérifier si un utilisateur à la role passé en paramètre, on return true si c'est vrai et false dans le cas contraire
    public function hasRoles($roles){
        if($this->roles()->where('libelle', $roles)->first())
        {
            return true;
        }
        return false;
    }
    // Relation Many to Many avec la table users (plusieurs utilisateurs peuvent avoir plusieurs conversation et l'inverse également)
    public function conversations()
    {
        return $this->belongsToMany('App\Conversation');
    }
    // Relation Many to One avec la table users (plusieurs posts appartiennent à un utilisateur)
    public function posts()
    {
        return $this->hasMany("App\Post");
    }
    // Relation Many to One avec la table users (plusieurs topic peuvent être créer par un utilisateur)
    public function topics()
    {
        return $this->hasMany("App\Topic");
    }
    // Relation Many to One avec la table users (plusieurs hobbies/passions appartiennent à un seul utilisateur)
    public function hobbiesPassions()
    {
        return $this->hasMany("App\HobbiesPassions");
    }
    // Relation Many to One avec la table users (plusieurs parcours pros appartiennent à un seul utilisateur)
    public function parcoursPros()
    {
        return $this->hasMany("App\ParCoursPro");
    }
    // Relation Many to One avec la table users (plusieurs message appartiennent à un utilisateur)
    public function messages()
    {
        return $this->hasMany('App\Message');
    }
    // Relation Many to One avec la table users (plusieurs signalements peuvent être fait par un utilisateur)
    public function signalements()
    {
        return $this->hasMany('App\Signalement');
    }
}
