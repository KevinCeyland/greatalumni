<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // Permet de pouvoir enregistré au modifié dans n'importe quel colonne de la table roles
    protected $guarded = [];
    // Relation Many to Many avec la table users (plusieurs utilisateurs peuvent avoir plusieurs rôles et l'inverse également)
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
