<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| CHEMIN DE BASE DE L'API : http://greatalumni/
|
*/


/*
* Groupe de route utilisant le préfix "auth" pour être appellé (exemple: .../api/auth/login)
* Route group dédié à l'authentification.
*/

Route::group(['prefix' => 'auth', 'namespace' => 'Auth'], function () {
    // Route de type POST permettant de se connecter
    Route::post('signin', 'SignInController')->name('login');
    // Route de type POST permettant de se déconnecter
    Route::post('signout', 'SignOutController')->name('signout');
    // Route de type GET permettant d'avoir des informations sur son profil (nom, prenom, adresse mail, etc..)
    Route::get('me', 'MeController')->name('me');
});
/**
 * Groupe de route au prefix api/annuaire/... concernant l'annuaire des anciens
 */
Route::group(['prefix' => 'annuaire'], function () {
    // Route de type GET permettant de récupérer la liste des utilisateurs dans l'annuaires avec comme paramètre un entier qui concerne la limite d'utilisateur à afficher
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::get('/index/{limit}', 'AnnuaireController@index')->middleware('auth');
    // Route de type GET permettant de chercher des utilisateurs dans l'annuaire
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::get('/search/{value}', "AnnuaireController@search")->middleware('auth');
});
/**
 * Groupe de route au préfix api/actualite/.. concernant les actualités
 */
Route::group(['prefix' => 'actualite'], function () {
    // Route de type GET permettant d'obtenir une liste d'actualites
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::get('/index/{limit}', 'ActualiteController@index')->name('actualite.index')->middleware('auth');
    // Route de type POST permettant d'enregistré une actualité
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::post('/store', 'ActualiteController@store')->name('actualite.store')->middleware('auth');
    // Route de type PUT permettant de modifier une actualité
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::put('/update/{id}', 'ActualiteController@update')->name('actualite.update')->middleware('auth');
    // Route de type DELETE permettant de supprimé une actualité
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::delete('/delete/{id}', 'ActualiteController@destroy')->name('actualite.delete')->middleware('auth');
    // Route de type GET permettant de réagir à une actualité
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::get('/like/{id}', 'ReactionController@likeActuPartage')->name('actualite.like')->middleware('auth');
    // Route de type POST permettant d'enregistrer un commentaire pour une actualité
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::post('/comments/{id}', 'CommentController@storeCommentActu')->name('actualite.storeCommentActu')->middleware('auth');
    // Route de type DELETE permettant de supprimé un commentaire d'une actualité
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::delete('/comments/delete/{id}', 'CommentController@deleteCommentActu')->name('actualite.deleteCommentActu')->middleware('auth');
});
/**
 * Groupe de route concernant les utilisateurs, prefix api/user/...
 */
Route::group(['prefix' => 'user'], function () {
    // Route de type GET permettant l'affichage du profil de l'utilisateur connecté
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::get('/profil', "UserController@profil")->middleware('auth');
    // Route de type GET permetant l'affichage de la liste des utilisateurs
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::get('/index', "UserController@index")->middleware('auth');
    // Route de type GET permetant l'affichage de la liste des utilisateurs avec comme paramètre un entier qui limite les utilisateurs à affiché
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::get('/listeUsers/{limit}', "UserController@listeUsers")->middleware('auth');
    // Route de type POST permettant l'update du profil utilisateur
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::post('/updateProfil', "UserController@updateProfil")->middleware('auth');
    // Route de type POST permettant d'enregistrer un utilisateur
    // Pour pouvoir utilisé cet route il faut être Administrateur (middleware admin)
    Route::post('/store', "UserController@store")->middleware('admin');
    // Route de type PUT permettant de mettre à jour un utilisateur
    // Pour pouvoir utilisé cet route il faut être Administrateur (middleware admin)
    Route::put('/update/{id}', "UserController@update")->middleware('admin');
    // Route de type DELETE permettant de supprimer un utilisateur
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::delete('/delete/{id}', "UserController@delete")->middleware('admin');

    /**
     * Groupe de route concernant le CRUD des hobbiesPassions des utilisateurs, prefix api/user/hobbiesPassions/...
     */
    Route::group(['prefix' => 'hobbiesPassions'], function () {
        Route::get('/index/{id}', "HobbiesPassionsController@index")->middleware('auth');
        Route::post('/store', "HobbiesPassionsController@store")->middleware('auth');
        Route::put('/update/{id}', "HobbiesPassionsController@update")->middleware('auth');
        Route::delete('/delete/{id}', "HobbiesPassionsController@delete")->middleware('auth');
    });
    /**
     * Groupe de route concernant le CRUD des parcoursPro des utilisateurs, prefix api/user/parcoursPro/...
     */
    Route::group(['prefix' => 'parcoursPro'], function () {
        Route::get('/index/{id}', "ParcoursProController@index")->middleware('auth');
        Route::post('/store', "ParcoursProController@store")->middleware('auth');
        Route::put('/update/{id}', "ParcoursProController@update")->middleware('auth');
        Route::delete('/delete/{id}', "ParcoursProController@delete")->middleware('auth');
    });
    /**
     * Groupe de route concernant la messagerie, prefix api/user/messagerie/...
     */
    Route::group(['prefix' => 'messagerie'], function () {
        // Route de type GET permet de récupérer la liste des utilisateurs trié par dernier message
        // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
        Route::get("/index", "MessagerieController@listeUsersOrderByLastMessage")->middleware('auth');
        // Route de type GET permet de créer une conversation avec l'id de l'utilisateur avec qui on souhaite discuté
        // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
        Route::get("/createConversations/{idUser}", "MessagerieController@createConversations")->middleware('auth');
        // Route de type POST permet d'enregistrer un message dans la conversation ayant l'id passé en paramètre
        // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
        Route::post('/storeMessage/{idConversation}', "MessagerieController@storeMessage")->middleware('auth');
        // Route de type GET permet d'avoir la liste de tous les messages de la conversation ayant l'id passé en paramètre
        // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
        Route::get('/listeMessage/{idConversation}', "MessagerieController@listeMessage")->middleware('auth');
    });
});
/**
 * Groupe de route concernant les signalements, prefix api/signalement/..
 */
Route::group(['prefix' => 'signalement'], function () {
    // Route de type POST permettant de signaler un utilisateur sur la base de son id
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::post("/report/{id}", "SignalementController@report")->middleware('auth');
    // Route de type GET permettant d'avoir la liste des signalements avec comme paramètre un entier qui concerne la limite de signalements à afficher
    // Pour pouvoir utilisé cet route il faut être Administrateur (middleware admin)
    Route::get("/listReportUser/{limit}", "SignalementController@listReportUser")->middleware('admin');
    // Route de type DELETE permettant de supprimer un signalement mais également l'élément signalé
    // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
    Route::delete('/deleteModel/{id}/{typeModel}', "SignalementController@deleteModel")->middleware('admin');
});

/**
 * Groupe de route concernant le forum, prefix api/forum/...
 */
Route::group(['prefix' => 'forum'], function () {

    /**
     * Groupe de route concernant les sujets, prefix api/forum/sujet/..
     */
    Route::group(['prefix' => 'sujet'], function () {
        // Route de type GET permettant de récupérer la liste des sujets
        // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
        Route::get("/index", "Forum\SujetController@index")->middleware('auth');
        /**
         * Groupe de route concernant les topics, prefix api/forum/sujet/topic/..
         */
        Route::group(['prefix' => 'topic'], function () {
            // Route de type GET permettant d'afficher les topic ayant pour sujet l'id passé en paramètre
            // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
            Route::get('/index/{id}', "Forum\TopicController@index")->middleware('auth');
            // Route de type POST permettant de créer un topic
            // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
            Route::post("/store", "Forum\TopicController@store")->middleware('auth');
            /**
             * Groupe de route concernant les posts, prefix api/forum/sujet/topic/post/..
             */
            Route::group(['prefix' => 'post'], function () {
                // Route de type GET permettant d'avoir la liste de tous les posts pour le topic ayant l'id passé en paramètre
                // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
                Route::get("/index/{id}", "Forum\PostController@index")->middleware('auth');
                // Route de type POST permettant de créer un post
                // Pour pouvoir utilisé cet route il faut être authentifier (middleware auth)
                Route::post("/store", "Forum\PostController@store")->middleware('auth');
            });
        });
    });
});
